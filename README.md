To run this app, install the dependencies using "npm install". Run the server
by doing "node index.js" from the main repo directory, and then "npm start"
from within the React app folder (cd react-peertopeerapp)